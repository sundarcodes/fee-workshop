import {Player} from './player';

export class Batsman extends Player {
    constructor(name: string) {
        super(name);
        this.runsScored = 0;
    }
    private runsScored: number;
    incrementRuns(runs: number) {
        this.runsScored += runs;
    }
    get runs(): number {
        return this.runsScored;
    }
}