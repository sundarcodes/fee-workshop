"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const player_1 = require("./player");
class Batsman extends player_1.Player {
    constructor(name) {
        super(name);
        this.runsScored = 0;
    }
    incrementRuns(runs) {
        this.runsScored += runs;
    }
    get runs() {
        return this.runsScored;
    }
}
exports.Batsman = Batsman;
