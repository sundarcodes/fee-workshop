import { Batsman } from "./batsman";

export class ScoreCard {
    batsmen: Batsman[];
    totalRuns: number;

    constructor() {
        this.batsmen = [];
        this.totalRuns = 0;
        this.batsmen.push(new Batsman('Kohli'));
        this.batsmen.push(new Batsman('Dhoni'));
    }

    generateScoreCard(runsArr: number[]) {
// 1. For every element in the array, keep adding it to a variable for 
// computing the total score
// 2. Check if the run scored is even or odd.
// 3. If even, increment current player's score by that number
// 4. If odd, increment current player's score and rotate strike
// 5. Check if over is completed.
// 6. If over completed, rotate strike.
// 7. If no more  elements, print the total score
// and individual player scores.
        let playerInStrike: Batsman = this.batsmen[0];
        for(let i = 0; i < runsArr.length; i++) {
            let runsScoredInThisBall = runsArr[i];
            this.totalRuns += runsScoredInThisBall;
            if (runsScoredInThisBall % 2 === 0) {
                playerInStrike.incrementRuns(runsScoredInThisBall);
            } else {
                playerInStrike.incrementRuns(runsScoredInThisBall);
                // rotate strike
                playerInStrike = this.findNextStriker(playerInStrike);
            }
            // Check over completion
            if ((i + 1) % 6 === 0) {
                playerInStrike = this.findNextStriker(playerInStrike);
            }
        }
        console.log(`Total Runs scored is: ${this.totalRuns}`);
        for (let i = 0; i < this.batsmen.length; i++) {
            console.log(`${this.batsmen[i].playerName} scored ${this.batsmen[i].runs}`)
        }
    }

    findNextStriker(playerInStrike: Batsman): Batsman {
        if (playerInStrike === this.batsmen[0]) {
            playerInStrike = this.batsmen[1]
        } else {
            playerInStrike = this.batsmen[0];
        }
        return playerInStrike;
    }
}